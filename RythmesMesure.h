#ifndef MIDITEST_RYTHMESMESURE_H
#define MIDITEST_RYTHMESMESURE_H

#include <array>
#include "contraintes_contrepoint.h"
#include <random>
#include <chrono>
#include "TestGenMelodie.h"
#include "GammeToMIDI.h"
#include "MIDIPlayer.h"
#include "MIDIMessagesList.h"
#include <thread>
#include <rtmidi/RtMidi.h>
#include <iostream>
/// Représente un rythme sur une mesure à 4 temps
enum RythmesMesure {
    // Rondes
    RONDE = 1,
    // Blanches
    BLANCHE_BLANCHE,
    BLANCHE_NOIRE_NOIRE,
    NOIRE_BLANCHE_NOIRE,
    NOIRE_NOIRE_BLANCHE,
    // Noires
    NOIRE_NOIRE_NOIRE_NOIRE,
    // Blanches chevauchant deux mesures
    DEMIBLANCHE1_BLANCHE_NOIRE,
    DEMIBLANCHE1_NOIRE_BLANCHE,
    DEMIBLANCHE1_NOIRE_NOIRE_NOIRE,
    BLANCHE_NOIRE_DEMIBLANCHE2,
    NOIRE_BLANCHE_DEMIBLANCHE2,
    NOIRE_NOIRE_NOIRE_DEMIBLANCHE2,
    DEMIBLANCHE1_NOIRE_NOIRE_DEMIBLANCHE2,
    DEMIBLANCHE1_BLANCHE_DEMIBLANCHE2
};

/// Retourne le nombre de notes correspondant à un rythme donné
int nombreDeNotes(RythmesMesure rythme);


#endif //MIDITEST_RYTHMESMESURE_H

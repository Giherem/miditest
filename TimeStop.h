//
// Created by jamian on 25/03/23.
//

#ifndef MIDITEST_TIMESTOP_H
#define MIDITEST_TIMESTOP_H

#include <gecode/search.hh>

class TimeStop : public Gecode::Search::Stop {
private:
    Gecode::Support::Timer _timer;
    unsigned int _limit;

public:
    TimeStop(unsigned int limit) : _limit(limit) {
        _timer.start();
    }

    bool stop(const Gecode::Search::Statistics &s, const Gecode::Search::Options &o) override
    {
        return _timer.stop() > _limit;
    }
};

#endif //MIDITEST_TIMESTOP_H

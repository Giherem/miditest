#pragma once

#include <vector>
/// Type représentant un message MIDI
typedef std::vector<unsigned char> MIDIMessage;

/// Type représentant la Hauteur d'une note dans le protocole MIDI
typedef unsigned char MidiPitch;
/// Type représentant la vélocité d'une note dans le protocole MIDI
typedef unsigned char MidiVelocity;
/// Type pour représenter le degré d'une note dans la gamme
typedef unsigned char NoteInScale;
/// Représente une note de musique, avec une hauteur et une durée
struct Note
{
    double duree; ///< Durée, exprimée en temps.
    MidiPitch hauteur; ///< hauteur MIDI de la note
    unsigned char velocite; ///< Vélocité MIDI d ela note
    /// Constructeur usuel
    Note(double d,MidiPitch h,MidiVelocity vel): duree(d),hauteur(h),velocite(vel){}
};
/// Fournit un message MIDI NOTE ON
inline MIDIMessage msgNoteON(MidiPitch hauteur,MidiVelocity velocite) {return MIDIMessage({0b10010000,hauteur,velocite});}
/// Fournit un message MIDI NOTE OFF
inline MIDIMessage msgNoteOFF(MidiPitch hauteur,MidiVelocity velocite) {return MIDIMessage({0b10000000,hauteur,velocite});}

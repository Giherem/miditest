//
// Created by jamian on 21/08/22.
//

#include "GammeToMIDI.h"

int GammeToMIDI::getNote(int note,int alteration) const
{
    int octave = 0;
    while (note > getLongueurGamme())
    {
        octave++;
        note -=getLongueurGamme();
    }
    while (note < 0)
    {
        octave--;
        note +=getLongueurGamme();
    }

    int degre = note % (int)mGamme.size();
    return mGamme[degre]+(octave*12)+alteration;
}

GammeToMIDI::GammeToMIDI(MidiPitch fondamentale, const int *deltaGamme, int longueurGamme)
{
    mGamme = vector<int>(longueurGamme);
    for (int i=0;i<longueurGamme;++i)
    {
        mGamme[i] = fondamentale + deltaGamme[i];
    }

}

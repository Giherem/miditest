//
// Created by jamian on 18/03/23.
//

#ifndef MIDITEST_CONTRAINTES_CONTREPOINT_H
#define MIDITEST_CONTRAINTES_CONTREPOINT_H

#include <vector>
using std::vector;

struct Note;

double isHarmonicIntervalAllowed(int interval);

double isMelodicIntervalAllowed(int interval);

double isParallelMotionForbidden(int interval1, int interval2);

// Pour vérifier le mouvement contraire, similaire et oblique, nous aurons besoin des intervalles mélodiques entre les notes adjacentes dans chaque voix.
double isMotionTypeAllowed(int melodicIntervalMain, int melodicIntervalSecondary);

bool isPreparedDissonance(const vector<Note>& mainNotes, const vector<Note>& secondaryNotes, int measure);

bool isResolvedDissonance(const vector<Note>& mainNotes, const vector<Note>& secondaryNotes, int measure);

double isMeasurePinpon(const Note *notes, int nbNotes);

#endif //MIDITEST_CONTRAINTES_CONTREPOINT_H

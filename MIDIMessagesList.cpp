//
// Created by jamian on 18/08/22.
//

#include <cassert>
#include "MIDIMessagesList.h"

bool MIDIMessagesList::addNote(const Note &n, int pos)
{
    int dureeSteps = mStepsParTemps * n.duree;
    if (dureeSteps == 0)
    {
        std::cout << "Warning : note trop courte " << n.duree << "Pour un découpage de " << mStepsParTemps
                  << " divisions par temps" << std::endl;
        return false;
    }
    assert(pos+dureeSteps < mList.size());
    mList[pos].emplace_back(msgNoteON(n.hauteur, n.velocite));
    mList[pos + dureeSteps].emplace_back(msgNoteOFF(n.hauteur, n.velocite));
    return true;
}

MIDIMessagesList::MIDIMessagesList(int nbTemps) : mList(nbTemps*24+1), mStepsParTemps(24)
{}

bool MIDIMessagesList::addNote(const Note &n, double posDebutTemps)
{
    return addNote(n,(int)(posDebutTemps*mStepsParTemps));
}

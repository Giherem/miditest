#include <iostream>
#include <rtmidi/RtMidi.h>
#include <thread>
#include "MIDIMessagesList.h"
#include "MIDIPlayer.h"
#include "GammeToMIDI.h"
#include <chrono>
#include <random>
#include "contraintes_contrepoint.h"
#include "PartitionCSP.h"
#include "TimeStop.h"

using namespace std;

void
generateThirdSpeciesCounterpoint(MIDIMessagesList &midiMessagesList, const GammeToMIDI &gamme, int numberOfMeasures,
                                 MidiVelocity velocity = 64)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    // Distribution pour choisir aléatoirement les notes de la gamme
    std::uniform_int_distribution<> noteDistribution(0, 6);
    // Distribution pour choisir aléatoirement l'octave
    std::uniform_int_distribution<> octaveDistribution(-1, 1);
    vector<Note> mainNotes(numberOfMeasures, Note(4.0, 60, velocity));
    vector<Note> secondaryNotes(numberOfMeasures * 4, Note(1.0, 60, velocity));
// Générer les notes principales en respectant les règles du contrepoint
    for (int mes = 0; mes < numberOfMeasures; ++mes) {
        double validity;
        double minValidity = 1. / 0.99;
        int i = mes * 4;
        do {
            minValidity *= 0.99;
            validity = 1.;
            int note = noteDistribution(gen);
            int octave = octaveDistribution(gen);
            MidiPitch newPitch = gamme.getNote(note) + 12 * octave;
            mainNotes[mes].hauteur = newPitch;

            if (mes > 0) {
                int melodicInterval =
                        static_cast<int>(mainNotes[mes].hauteur) - static_cast<int>(mainNotes[mes - 1].hauteur);
                validity *= isMelodicIntervalAllowed(melodicInterval);
            }

            for (int j = i; j < i + 4; ++j) {
                int noteSec = noteDistribution(gen);
                int octaveSec = octaveDistribution(gen);
                MidiPitch newPitchSec = gamme.getNote(noteSec) + 12 * octaveSec;
                secondaryNotes[j].hauteur = newPitchSec;
                int measure = i / 4;
                int harmonicInterval =
                        static_cast<int>(secondaryNotes[j].hauteur) - static_cast<int>(mainNotes[measure].hauteur);
                validity *= isHarmonicIntervalAllowed(harmonicInterval);

                if (j > 0) {
                    int melodicIntervalSecondary = static_cast<int>(secondaryNotes[j].hauteur) -
                                                   static_cast<int>(secondaryNotes[j - 1].hauteur);
                    int melodicIntervalMain = 0;
                    if (j % 4 == 0) {
                        melodicIntervalMain = static_cast<int>(mainNotes[measure].hauteur) -
                                              static_cast<int>(mainNotes[measure - 1].hauteur);
                    }

                    validity *= isMelodicIntervalAllowed(melodicIntervalSecondary);
                    validity *= isMotionTypeAllowed(melodicIntervalMain, melodicIntervalSecondary);
                    if (mainNotes[measure].hauteur > secondaryNotes[j].hauteur) validity *= 0.1;
                }
            }
            validity *= isMeasurePinpon(&secondaryNotes[i], 4);
            cout << minValidity << " - " << validity << endl;
        } while (validity < minValidity);
    }
// TODO : vérifier et appliquer les règles de préparation et de résolution des dissonances

// Recopie de l'ensemble des notes principales dans la liste de messages MIDI
    for (int i = 0; i < mainNotes.size(); ++i) {
        midiMessagesList.addNote(mainNotes[i], i * 4.0);
    }
// Recopie de l'ensemble des notes secondaires dans la liste de messages MIDI
    for (int i = 0; i < secondaryNotes.size(); ++i) {
        midiMessagesList.addNote(secondaryNotes[i], i * 1.0);
    }
}

int main()
{
    // Créer une gamme de Do majeur
    GammeToMIDI gamme(60, true);

    int nbMesures = 4;
    int sigTemps = 4;
    int nbVoix = 4;
    MIDIMessagesList midiMessagesList(nbMesures * sigTemps);
    PartitionCSP problem(nbMesures,nbVoix,sigTemps);
    problem.contrepoint4Voix();
    TimeStop stop(10000);
    Search::Options options;
    options.stop=&stop;
    options.threads=4;
    BAB<PartitionCSP> bab(&problem,options);
    PartitionCSP *solution = nullptr;

    while (PartitionCSP *nextSolution = bab.next()) {
        if (solution != nullptr) {
            delete solution;
        }
        solution = nextSolution;
        cout<<"Score : "<<solution->penalty.min()<<"-"<<solution->penalty.max()<<endl;
    }
    if (!solution)
    {
        cout<<"A pas solution... "<<endl;
        abort();
    }
    auto mml = solution->getSolution();
    midiMessagesList = *mml;

    // Générer du contrepoint rigoureux de la troisième espèce et le stocker dans midiMessagesList
//    generateThirdSpeciesCounterpoint(midiMessagesList, gamme, nbMesures);

    vector<unsigned char> message;
    message.reserve(3);
    unsigned int portNumber;
    unique_ptr<RtMidiOut> midiout = nullptr;
    // Check outputs.
    string portName;
    unsigned int nPorts;
    // RtMidiOut constructor
    try {
        midiout = std::make_unique<RtMidiOut>();
    }
    catch (RtMidiError &error) {
        error.printMessage();
        exit(EXIT_FAILURE);
    }
    // Check outputs.
    nPorts = midiout->getPortCount();
    cout << "\nThere are " << nPorts << " MIDI output ports available.\n";
    for (unsigned int i = 0; i < nPorts; i++) {
        try {
            portName = midiout->getPortName(i);
        }
        catch (RtMidiError &error) {
            error.printMessage();
            cout << "Aie ! " << endl;
            return 1;
        }
        cout << "  Output Port #" << i << ": " << portName << '\n';
    }
    cout << '\n';

    cout << "Please select port" << endl;
    cin >> portNumber;
    cout << "POrt " << portNumber << " selected." << endl;

    // Open first available port.
    midiout->openPort(portNumber);
    // Send out a series of MIDI messages.

    // Program change: 192, 5
    message.push_back(192);
    message.push_back(19);
    midiout->sendMessage(&message);
    // Control Change: 176, 7, 100 (volume)
    message.clear();
    message.push_back(176);
    message.push_back(7);
    message.push_back(127);
    midiout->sendMessage(&message);

    MIDIPlayer player(std::move(midiout));
    int tempo = 100;

    player.setTempo(tempo);
    player.addMessageList(std::make_unique<MIDIMessagesList>(midiMessagesList));
    player.run();

    this_thread::sleep_for(chrono::seconds(1 * (nbMesures * 4) * 60 / tempo + 1));
    player.stop();
    this_thread::sleep_for(chrono::seconds(2));

    cout << "Thread principal j'ai mouru" << endl;

    return 0;
}

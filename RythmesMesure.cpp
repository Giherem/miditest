#include "RythmesMesure.h"

int nombreDeNotes(RythmesMesure rythme)
{
    switch (rythme)
    {
        case RONDE:
            return 1;
        case BLANCHE_BLANCHE:
            return 2;
        case BLANCHE_NOIRE_NOIRE:
        case NOIRE_BLANCHE_NOIRE:
        case NOIRE_NOIRE_BLANCHE:
        case DEMIBLANCHE1_BLANCHE_NOIRE:
        case DEMIBLANCHE1_NOIRE_BLANCHE:
        case DEMIBLANCHE1_BLANCHE_DEMIBLANCHE2:
            return 3;
        case NOIRE_NOIRE_NOIRE_NOIRE:
        case DEMIBLANCHE1_NOIRE_NOIRE_NOIRE:
        case BLANCHE_NOIRE_DEMIBLANCHE2:
        case NOIRE_BLANCHE_DEMIBLANCHE2:
        case NOIRE_NOIRE_NOIRE_DEMIBLANCHE2:
        case DEMIBLANCHE1_NOIRE_NOIRE_DEMIBLANCHE2:
            return 4;
        default:
            abort();
    }
}

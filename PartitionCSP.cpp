#include "PartitionCSP.h"
#include "MIDIMessagesList.h"

PartitionCSP::PartitionCSP(int nombreDeMesures, int nombreDeVoix, int tempsParMesure)
        : Space(),
          mNombreDeMesures(nombreDeMesures),
          mNombreDeVoix(nombreDeVoix),
          mTempsParMesure(tempsParMesure),
          mRythmes(nombreDeVoix),
          mHauteurs(nombreDeVoix),
          mNombreDeNotes(nombreDeVoix),
          penalty(*this,0,Int::Limits::max)
{
    for (int voix = 0; voix < mNombreDeVoix; voix++)
    {
        mRythmes[voix] = BoolVarArray(*this, mNombreDeMesures * mTempsParMesure,0,1);
        mHauteurs[voix] = IntVarArray(*this, mNombreDeMesures * mTempsParMesure, 0, 127);
        mNombreDeNotes[voix] = IntVarArray(*this, mNombreDeMesures, 0, mTempsParMesure+1);

        auto &rythmes = mRythmes[voix];
        auto &hauteurs = mHauteurs[voix];
        auto &nombreDeNotes = mNombreDeNotes[voix];

        // On coupe toujours la note finale
//        rel(*this,rythmes[rythmes.size()-1] == true);

        for (int i = 0; i < mNombreDeMesures * mTempsParMesure - 1; ++i)
        {
            BoolVar rythme = rythmes[i];

            IntVar hauteurAvant = hauteurs[i];
            IntVar hauteurApres = hauteurs[i + 1];
            rel(*this, (!rythme) >> (hauteurAvant == hauteurApres));
        }

        for (int i = 0; i < mNombreDeMesures; ++i)
        {
            IntVar nbNotes = nombreDeNotes[i];
            BoolVarArgs coupuresMesures;
            for (int t = 0; t < mTempsParMesure; ++t)
            {
                coupuresMesures << rythmes[i * mTempsParMesure + t];
            }
            rel(*this, nbNotes == sum(coupuresMesures));
        }
    }

    srand(time(nullptr));

    for (int mesure = 0; mesure < mNombreDeMesures; ++mesure)
    {
        for (int voix = 0; voix < mNombreDeVoix; ++voix)
        {
            IntVarArgs h, nn;
            BoolVarArgs r;
            for (int t = 0; t < mTempsParMesure; ++t)
            {
                h << mHauteurs[voix][mesure * mTempsParMesure + t];
                r << mRythmes[voix][mesure * mTempsParMesure + t];
            }
            nn << mNombreDeNotes[voix][mesure];
            branch(*this, r, BOOL_VAR_RND(Rnd(rand())), BOOL_VAL_RND(Rnd(rand())));
            branch(*this, h, INT_VAR_SIZE_MIN(), INT_VAL_RND(Rnd(rand())));
            branch(*this, nn, INT_VAR_SIZE_MIN(), INT_VAL_MIN());
        }
    }
}

PartitionCSP::PartitionCSP(PartitionCSP &s)
        : Space(s),
          mNombreDeMesures(s.mNombreDeMesures),
          mNombreDeVoix(s.mNombreDeVoix),
          mTempsParMesure(s.mTempsParMesure),
          mRythmes(s.mRythmes),
          mHauteurs(s.mHauteurs),
          mNombreDeNotes(s.mNombreDeNotes)
{
    for (int voix = 0; voix < mNombreDeVoix; voix++)
    {
        mRythmes[voix].update(*this, s.mRythmes[voix]);
        mHauteurs[voix].update(*this, s.mHauteurs[voix]);
        mNombreDeNotes[voix].update(*this, s.mNombreDeNotes[voix]);
    }
    penalty.update(*this,s.penalty);
}

std::unique_ptr<MIDIMessagesList> PartitionCSP::getSolution()
{
    auto ret = make_unique<MIDIMessagesList>(mNombreDeMesures * mTempsParMesure);
    for (int voix = 0; voix < mNombreDeVoix; ++voix)
    {
        auto &h = mHauteurs[voix];
        auto &r = mRythmes[voix];
        for (int i = 0; i < h.size(); ++i)
        {
            int tempsDebut = i;
            int hauteur = h[i].val();
            int nbTemps = 1;
            while (i < h.size()-1 && r[i].val() == false)
            {
                ++i;
                ++nbTemps;
            }
            cout<<nbTemps<<" "<<hauteur<<" - ";
            ret->addNote(Note(nbTemps, hauteur, 100), (double) tempsDebut);
        }
        cout<<endl;
    }

    return ret;
}

void PartitionCSP::contrepoint4Voix()
{
    // Gamme Do majeur

    IntSet doMajeur({0, 2, 4, 5, 7, 9, 11});
    for (auto &hauteurs: mHauteurs)
        for (int i = 0; i < mNombreDeMesures * mTempsParMesure; ++i)
        {
            IntVar &hauteur = hauteurs[i];
            IntVar noteMod12(*this, 0, 11);
            rel(*this, hauteur % 12 == noteMod12);
            dom(*this, noteMod12, doMajeur);
        }

/*
    // Toutes les notes à 1 temps
    for (auto& tab : mRythmes)
        for (auto& r : tab)
            rel(*this,r == true);
*/
    // Rythmes dex voix : Alto, tenor et basse en rondes.
    for (int temps=0; temps < mNombreDeMesures*mTempsParMesure;++temps)
    {
        rel(*this,mRythmes[1][temps] == ((temps % mTempsParMesure) ==3));
        rel(*this,mRythmes[2][temps] == ((temps % mTempsParMesure) ==3));
        rel(*this,mRythmes[3][temps] == ((temps % mTempsParMesure) ==3));
        rel(*this,mRythmes[0][temps] == true);
    }
    // Tessitures des voix (0 : soprano, 1 : alto, 2 : tenor, 3 : basse)
    for (int temps = 0; temps < mNombreDeMesures * mTempsParMesure; ++temps)
    {
        rel(*this, mHauteurs[0][temps] >= 60);
        rel(*this, mHauteurs[0][temps] <= 81);

        rel(*this, mHauteurs[1][temps] >= 52);
        rel(*this, mHauteurs[1][temps] <= 72);

        rel(*this, mHauteurs[2][temps] >= 48);
        rel(*this, mHauteurs[2][temps] <= 69);

        rel(*this, mHauteurs[3][temps] >= 36);
        rel(*this, mHauteurs[3][temps] <= 57);
    }

    // Harmoniques consonants entre les voix

    for (int temps = 0; temps < mNombreDeMesures * mTempsParMesure; ++temps) {
        for (int v1 = 0; v1 < mNombreDeVoix - 1; ++v1) {
            for (int v2 = v1 + 1; v2 < mNombreDeVoix; ++v2) {
                IntVar intervalle = expr(*this, abs(mHauteurs[v1][temps] - mHauteurs[v2][temps]));
                IntVar intervalleMod7 = expr(*this, intervalle % 7);
                IntSet consonances({0, 3, 4, 5, 7, 8});
                dom(*this, intervalleMod7, consonances);
            }
        }
    }


    // Éviter les quintes et octaves par mouvement direct

    for (int temps = 0; temps < (mNombreDeMesures * mTempsParMesure) - 1; ++temps) {
        for (int v1 = 0; v1 < mNombreDeVoix - 1; ++v1) {
            for (int v2 = v1 + 1; v2 < mNombreDeVoix; ++v2) {
                IntVar intervalle1 = expr(*this, abs(mHauteurs[v1][temps] - mHauteurs[v2][temps]));
                IntVar intervalle2 = expr(*this, abs(mHauteurs[v1][temps + 1] - mHauteurs[v2][temps + 1]));
                rel(*this, (intervalle1 != 7) || (intervalle2 != 7));
                rel(*this, (intervalle1 != 12) || (intervalle2 != 12));
            }
        }
    }


    // Privilégie les déplacements par mouvements conjoints
    IntVarArgs penalties;

    for (int temps = 0; temps < mNombreDeMesures * mTempsParMesure - 1; ++temps) {
        for (int voix = 0; voix < mNombreDeVoix - 1; ++voix) {
            IntVar interval = expr(*this, mHauteurs[voix][temps] - mHauteurs[voix + 1][temps]);
            IntVar peno = intervalPenalty(*this, interval);
            penalties << peno;
        }
    }

    rel(*this, penalty == sum(penalties));


    // Pas de superposition ou de croisement des voix

    for (int temps = 0; temps < mNombreDeMesures * mTempsParMesure; ++temps) {
        for (int voix = 0; voix < mNombreDeVoix - 1; ++voix) {
            rel(*this, mHauteurs[voix][temps] > mHauteurs[voix + 1][temps]);
        }
    }


    // Cadence finale


//    IntVar b_dominante = expr(*this, mHauteurs[mNombreDeVoix - 1][(mNombreDeMesures * mTempsParMesure) - 1 - 4] % 12);
//    IntVar b_tonique = expr(*this, mHauteurs[mNombreDeVoix - 1][(mNombreDeMesures * mTempsParMesure) - 1] % 12);

//    IntVar s_sensible = expr(*this, mHauteurs[0][(mNombreDeMesures * mTempsParMesure) - 2] % 12);
//    IntVar s_tonique = expr(*this, mHauteurs[0][(mNombreDeMesures * mTempsParMesure) - 1] % 12);

//    rel(*this, b_dominante == 7); // La basse doit se déplacer de la dominante (sol en do majeur)
//    rel(*this, b_tonique == 0); // La basse doit se déplacer à la tonique (do en do majeur)

//    rel(*this, s_sensible == 11); // La mélodie doit se déplacer de la sensible (si en do majeur)
//    rel(*this, s_tonique == 0); // La mélodie doit se déplacer à la tonique (do en do majeur)

    // Position initiale
    // Petit truc perso, (basse en Do, ténor en Sol)
//    IntVar t_bassei = expr(*this, mHauteurs[mNombreDeVoix - 1][0] % 12);
//    IntVar t_tnori = expr(*this, mHauteurs[mNombreDeVoix - 2][0] % 12);
//    rel(*this, t_bassei == 7);
//    rel(*this, t_tnori == 0);

}

IntVar PartitionCSP::intervalPenalty(Space &space, IntVar interval)
{
    IntVar peno(space, 0, 100);

    rel(space, (interval == 0) >> (peno == 10)); // uni
    rel(space, (interval == 1) >> (peno == 0)); // sec min
    rel(space, (interval == 2) >> (peno == 0)); // sec maj
    rel(space, (interval == 3) >> (peno == 0)); // tie min
    rel(space, (interval == 4) >> (peno == 0)); // tie maj
    rel(space, (interval == 5) >> (peno == 1)); // quarte
    rel(space, (interval == 6) >> (peno == 100)); // TRITON
    rel(space, (interval == 7) >> (peno == 1)); // quinte
    rel(space, (interval == 8) >> (peno == 2)); // six min
    rel(space, (interval == 9) >> (peno == 100)); // six maj
    rel(space, (interval == 10) >> (peno == 100)); // spt min
    rel(space, (interval == 11) >> (peno == 100)); // spt maj
    rel(space, (interval == 12) >> (peno == 2)); // oct

    rel(space,interval<=12); // interdit les sauts > octave

    return peno;
}

//
// Created by jamian on 18/03/23.
//

#include "contraintes_contrepoint.h"
#include <cmath>
#include <algorithm>
#include "definitions.h";
double isHarmonicIntervalAllowed(int interval)
{
    interval = abs(interval);
    switch (interval) {
        case 0:   // Unisson
        case 3:   // Tierce mineure
        case 4:   // Tierce majeure
        case 7:   // Quinte juste
        case 8:   // Sixte mineure
        case 9:   // Sixte majeure
        case 12:  // Octave
            return 1.;
        default:
            return 0.01;
    }
}

double isMelodicIntervalAllowed(int interval)
{
    interval = abs(interval);
    switch (interval)
    {
        case 1:
        case 2:
            return 1.; // Mouvement conjoint OK
        case 3:
        case 4:
            return 0.9; // tierces OK
        case 5:
        case 7:
            return 0.5; // Intervalles justes, oui mais bof
        case 8:
        case 12:
            return 0.3; // Sixte et octave, encore plus bof
        default:
            return 0.;
    }
}

double isParallelMotionForbidden(int interval1, int interval2)
{
    int absInterval1 = abs(interval1);
    int absInterval2 = abs(interval2);
    return !((absInterval1 == 0 || absInterval1 == 7 || absInterval1 == 12) &&
             (absInterval1 == absInterval2));
}

double isMotionTypeAllowed(int melodicIntervalMain, int melodicIntervalSecondary)
{
    // Mouvement contraire
    if ((melodicIntervalMain < 0 && melodicIntervalSecondary > 0) || (melodicIntervalMain > 0 && melodicIntervalSecondary < 0)) {
        return 1.;
    }
    // Mouvement similaire et oblique
    return (abs(melodicIntervalMain) <= 2 && abs(melodicIntervalSecondary) <= 2)?0.95:0.1;
}

double isMeasurePinpon(const Note *notes, int nbNotes)
{
    const Note& note1=notes[0],note2=notes[1],note3=notes[2],note4=notes[3];
    if (note1.hauteur==note3.hauteur && note2.hauteur==note4.hauteur) return 0.1;
    if (note1.hauteur < note2.hauteur && note2.hauteur < note3.hauteur && note3.hauteur < note4.hauteur) return 1.;
    if (note1.hauteur > note2.hauteur && note2.hauteur > note3.hauteur && note3.hauteur > note4.hauteur) return 1.;
    return 0.5;
}

bool isPreparedDissonance(const vector <Note> &mainNotes, const vector <Note> &secondaryNotes, int measure)
{
    // Comme il s'agit de la troisième espèce, nous devons vérifier si la note secondaire est une dissonance et si elle est préparée par une consonance.
    // TODO : implémenter la vérification de la préparation de la dissonance pour la troisième espèce.
    return true;
}

bool isResolvedDissonance(const vector <Note> &mainNotes, const vector <Note> &secondaryNotes, int measure)
{
    // Comme il s'agit de la troisième espèce, nous devons vérifier si la note secondaire est une dissonance et si elle est résolue par une consonance.
    // TODO : implémenter la vérification de la résolution de la dissonance pour la troisième espèce.
    return true;
}

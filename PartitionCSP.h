#pragma once

#include <gecode/int.hh>
#include <gecode/minimodel.hh>
#include <gecode/search.hh>

#include <iostream>
#include <vector>
#include <memory>

#include "RythmesMesure.h"

class MIDIMessagesList;

using namespace Gecode;

class PartitionCSP : public Space
{
public:
    /// Nombre de mesures de la partition
    const int mNombreDeMesures;
    /// Nombre devoix de la partition
    const int mNombreDeVoix;
    /// Nombre de temps par mesure
    const int mTempsParMesure;
    /// Rythmes de chaque voix. POur la voix i, mRythmes[i][j] indique s'il y a un changement de note à la fin du
    /// j-ème temps.
    std::vector<BoolVarArray> mRythmes;
    /// mHauteur[i][j] : hauteur de la note chantée par la i-ème voix au j-ème temps.
    std::vector<IntVarArray> mHauteurs;
    /// mNombreDeNotes[i][m] : nombre de notes de la i-ème voix à la mesure m (une mesure dure 4 temps)
    std::vector<IntVarArray> mNombreDeNotes;
    /// Variable de pénalité si besoin
    IntVar penalty;
    /// Constructeur usuel
    explicit PartitionCSP(int nombreDeMesures = 32, int nombreDeVoix = 4, int tempsParMesure = 4);
    /// Constructeur de copie nécessaire pour la recherche
    PartitionCSP(PartitionCSP &s);
    /// Méthode de copie nécessaire pour la recherche
    Space *copy() override
    {
        return new PartitionCSP(*this);
    }

    std::unique_ptr<MIDIMessagesList> getSolution();

    /// Pste les contraintes régissant le contrepoint à 4 voix (1ere espèce, on considère que les rondes sont destemps)
    void contrepoint4Voix();

    IntVar intervalPenalty(Space &space, IntVar interval);
    void constrain(const Space& _best) override {
        const auto& best = static_cast<const PartitionCSP&>(_best);
        rel(*this, penalty < best.penalty.val());
    }
};

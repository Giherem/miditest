//
// Created by jamian on 21/08/22.
//

#include "TestGenMelodie.h"

double TestGenMelodie::getScoreEcart(int noteAvant, int noteApres) const
{
    double scoresEcarts[12]={
            1.,  // Unisson
            0.7,  //2nde mineure
            0.7,  // 2nde majeure
            0.5,  // 3rce mineure
            0.5,  // 3rce majeure
            0.2,  // Quarte juste
            0.01,  // Triton
            0.2,  // Quinte juste
            0.1,  // Sixte mineure
            0.1,  // sixte majeure
            0.05, // septieme mineure
            0.05  // septieme majeure
    };
    // On va faiere le produit d'un score fonction de l'intervalle et d'un score fonction de la distance absolue
    int absDiff = abs(noteApres-noteAvant);
    // Distance absolue : pénalité au carré de la différence d'octave+1
    double scoreDistance = 1./pow(1.+(double)(absDiff/12),3);

    // Intervalle
    int ecart = absDiff%12;
    double scoreEcart = scoresEcarts[ecart];

    return scoreEcart*scoreDistance;

}

TestGenMelodie::TestGenMelodie(const GammeToMIDI &gamme) :mGamme(gamme)
{
    srand(time(nullptr));
}

int TestGenMelodie::nextNote()
{
    constexpr int etendue=21;
    double distri[etendue];
    double somme=0;
    cout<<"NA : "<<noteActuelle<<" - ";
    cout<<"distri : ";
    for (int i=0;i<etendue;++i)
    {
        distri[i] = getScoreEcart(mGamme.getNote(noteActuelle),mGamme.getNote(i));
        cout<<distri[i]<<" ";
        somme +=  distri[i];
    }
    cout<<endl;
    int rnd = rand()%131072;
    double drnd = ((double)(rnd))*somme/131072.;
    int noteSuivante = 0;
    while(drnd > distri[noteSuivante])
    {
        drnd -= distri[noteSuivante];
        ++noteSuivante;
    }
    noteActuelle=noteSuivante;
    return noteSuivante;

}

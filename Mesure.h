#ifndef MIDITEST_MESURE_H
#define MIDITEST_MESURE_H

#include <array>
#include "RythmesMesure.h"
#include "contraintes_contrepoint.h"
#include <random>
#include <chrono>
#include "TestGenMelodie.h"
#include "GammeToMIDI.h"
#include "MIDIPlayer.h"
#include "MIDIMessagesList.h"
#include <thread>
#include <rtmidi/RtMidi.h>
#include <iostream>

class Mesure {
    RythmesMesure rythme;
    array<int, 4> notes{-1,-1,-1,-1};

public:
    explicit Mesure(RythmesMesure rythme) : rythme(rythme) {
        int nbNotes = nombreDeNotes(rythme);
        for (int i = 0; i < 4; ++i) {
            notes[i] = (i < nbNotes) ? 60 : -1; // Initialiser avec des Do du milieu (MIDI 60)
        }
    }
    bool estCompatibleAvec(const Mesure& autreMesure) const {
        // Vérifier si la dernière note de la mesure actuelle est une demiblanche2
        if (rythme == BLANCHE_NOIRE_DEMIBLANCHE2 ||
            rythme == NOIRE_BLANCHE_DEMIBLANCHE2 ||
            rythme == NOIRE_NOIRE_NOIRE_DEMIBLANCHE2 ||
            rythme == DEMIBLANCHE1_NOIRE_NOIRE_DEMIBLANCHE2 ||
            rythme == DEMIBLANCHE1_BLANCHE_DEMIBLANCHE2) {
            // Si la première note de l'autre mesure est une demiblanche1 et a la même hauteur
            if ((autreMesure.rythme == DEMIBLANCHE1_BLANCHE_NOIRE ||
                 autreMesure.rythme == DEMIBLANCHE1_NOIRE_BLANCHE ||
                 autreMesure.rythme == DEMIBLANCHE1_NOIRE_NOIRE_NOIRE) &&
                notes[nombreDeNotes(rythme) - 1] == autreMesure.notes[0]) {
                return true;
            } else {
                return false;
            }
        }
        // Les autres mesures sont toujours compatibles
        return true;
    }
};

#endif //MIDITEST_MESURE_H

#pragma once
#include <thread>
#include <rtmidi/RtMidi.h>
#include <memory>
#include <chrono>
#include <list>
#include "MIDIMessagesList.h"
#include <mutex>

using std::unique_ptr;
using std::list;
using std::chrono::time_point;
using std::chrono::steady_clock;
using std::chrono::duration;
/**
 * Gère un thread qui va envoyer sur un port midi la musique qu'on lui fournit.
 */
class MIDIPlayer
{
public:
    explicit MIDIPlayer(unique_ptr<RtMidiOut>&& midiOut);
    ~MIDIPlayer()=default;
    void setTempo(int tempo);///< définit le tempo
    void addMessageList(unique_ptr<MIDIMessagesList>&& msg);
    void run(); ///< lance la musique
    void stop(); ///< arrête la musique à la fin de la liste en cours
    int size() const{return mBufferSize;} ///< Taille de la file d'attente

    bool getStatus() const; ///< true : running ; false : stoppé

private:
    /// Joue une MIDIMessagesList du buffer (s'il n'est pas vide)
    bool thr_OneStep();
    /// Fonction exécutée par le thread. Boucle tant qu'il y a du boulot et qu'on ne lui demande pas de s'arrêter
    void thr_main();
    /// Envoie les derniers messages de precMML et le reset.
    void clearPrecMML();
    list<unique_ptr<MIDIMessagesList>> mBuffer;
    int mBufferSize=0;
    /// POrt MIDI sur lequel on envoie les commandes
    unique_ptr<RtMidiOut> mMIDIPort;
    /// Tempo actuel (pris en compte à chaque début de traitement d'un élément du buffer)
    int mTempo=120;
    /// Mutex protégeant la liste d'attente
    std::mutex mtxBuffer;
    /// Le thread musicien
    std::thread mWorker;
    /// True : on veut que ça joue. false : on veut que ça s'arrête.
    bool wantedStatus=true;
    bool isRunning=false;
    time_point<steady_clock,duration<double>> timeLastPlayed;
    unique_ptr<MIDIMessagesList> precMML;
    MIDIMessage mMsgSynchro = {248};
};

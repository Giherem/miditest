#pragma once

#include "definitions.h"
#include <iostream>
using std::vector;
/**
 * Liste de messages MIDI.
 * Représente une liste de message MIDI à envoyer à intervalles réguliers sur un port MIDI. Chaque intervalle de temps élémentaires fait 1/24 temps (musical).
 */
class MIDIMessagesList
{
public:
    /// Créé une liste de messages MIDI d'une longueur donnée.
    /// @param nbTemps Nombre de temps musicaux de la liste (la liste aura donc 24*nbTemps intervalles de temps élémentaires)
    explicit MIDIMessagesList(int nbTemps);
    /// constructeur par recopie usuel
    MIDIMessagesList(const MIDIMessagesList&) = default;
    /**
     * Ajoute une note à la liste, en ajoutant aux bons endroit les signaux NOTE ON et NOTE OFF correspondants.
     * @param n             note à ajouter
     * @param posDebutTemps position temporelle (en temps musicaux) à laquelle ajouter la note
     */
    bool addNote(const Note& n,double posDebutTemps);
    /// Retourne le venteur contenant les messages MIDI à envoyer
    const vector<vector<MIDIMessage >>& getList() const {return mList;}
    /// Nombre d'intervalles de temps élémentaires par temps musical (24 par défaut).
    int getStepsParTemps() const {return mStepsParTemps;}
private:
    bool addNote(const Note& n,int posDebutInDiv);
    int mStepsParTemps;
    vector<vector<MIDIMessage>> mList;
};

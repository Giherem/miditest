#include "MIDIPlayer.h"
#include <chrono>

void MIDIPlayer::setTempo(int tempo)
{
    mTempo=tempo;
}

MIDIPlayer::MIDIPlayer(unique_ptr<RtMidiOut> &&midiOut) : mMIDIPort(std::move(midiOut)){}

bool MIDIPlayer::thr_OneStep()
{
    unique_ptr<MIDIMessagesList> mml;
    {
        std::unique_lock<std::mutex> lck(mtxBuffer);
        if (mBuffer.empty()) return false;
        mml = std::move(mBuffer.front());
        mBuffer.pop_front();
        mBufferSize = mBuffer.size();
    }

    int dureeTempsMicrosec = 60'000'000/mTempo;
    auto dureeEntreSteps = std::chrono::microseconds(dureeTempsMicrosec/mml->getStepsParTemps());

    for (int i=0;i<mml->getList().size()-1;++i)
    {
        auto& msgs=mml->getList()[i];
        timeLastPlayed+=dureeEntreSteps;
        std::this_thread::sleep_until(timeLastPlayed);
        if (i==0) clearPrecMML();
        mMIDIPort->sendMessage(&mMsgSynchro);
        for (auto& msg :msgs)
            mMIDIPort->sendMessage(&msg);
    }
    precMML=std::move(mml);
    return true;
}

void MIDIPlayer::thr_main()
{
    isRunning=true;
    timeLastPlayed=steady_clock::now();
    // Dans un premier temps, on va faire de la gestion crade : si le buffer est vide, on se met en attente active.
    // On fera plus propre après (ou pas...)
    while (wantedStatus)
    {
        bool ok = thr_OneStep();
        if (!ok) std::this_thread::sleep_for(std::chrono::seconds(1));
    }
    clearPrecMML();
    isRunning=false;
    std::cout<<"Thread musicien j'ai mouru"<<std::endl;
}

void MIDIPlayer::stop()
{
    wantedStatus=false;
}

void MIDIPlayer::run()
{
    if (isRunning) return;
    std::thread thr([this](){this->thr_main();});
    thr.detach();
}

bool MIDIPlayer::getStatus() const
{
    return isRunning;
}

void MIDIPlayer::addMessageList(unique_ptr<MIDIMessagesList> &&msg)
{
    std::unique_lock<std::mutex> lck(mtxBuffer);
    mBuffer.emplace_back(std::move(msg));
    mBufferSize = mBuffer.size();

}

void MIDIPlayer::clearPrecMML()
{
    if (precMML)
    {
        for (auto &msg: precMML->getList().back())
            mMIDIPort->sendMessage(&msg);
        precMML.reset();
    }
}

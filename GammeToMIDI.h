#pragma once
#include <vector>
#include "definitions.h"
using namespace std;

/// Calcule, à partir d'une gamme mineure ou majeure, d'une octave et d'un degré de note, la hauteur MIDI correspondante.
class GammeToMIDI
{
public:
    /**
     * COnstructeur permettant de définir une gamme personnalisée en fournissant les décalages des notes par rapport à la fondamentale
     * @param fondamentale  hauteur MIDI de la note foncamentale de la gamme
     * @param deltaGamme    tableau des décalages des notes de la gamme par rapport à la fondamentale
     * @param longueurGamme nombre de notes de la gamme (longueur du tableau deltaGamme)
     */
    GammeToMIDI(MidiPitch fondamentale,const int* deltaGamme, int longueurGamme);
    /**
     * Constructeur permettant de définir une gamme mineure ou majeure
     * @param fondamentale  Hauteur MIDI de la note fondamentale
     * @param mineur        true: gamme mineure, false: gamme majeure
     */
    GammeToMIDI(MidiPitch fondamentale,bool mineur) : GammeToMIDI(fondamentale,mineur?deltaMineur:deltaMajeur,7)
    {}
    /// COnstructeur par défaut, définit une gamme de Do majeur.
    GammeToMIDI() : GammeToMIDI(60,false){}

    /**
     * Retourne la hauteur d'une note MIDI à partir
     * @param note
     * @param alteration
     * @return
     */
    int getNote(int note, int alteration=0) const;

private:
    vector<int> mGamme;
    int getLongueurGamme() const {return (int)mGamme.size();}

    const int deltaMajeur[7]{0,2,4,5,7,9,11}; ///< Décalages pour une gamme majeure
    const int deltaMineur[7]{0,2,3,5,7,8,10}; ///< Décalages pour une gamme mineure
};

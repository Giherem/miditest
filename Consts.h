//
// Created by jamian on 07/03/2021.
//

#pragma once

constexpr char DOb=-1;
constexpr char DO=0;
constexpr char DOd=1;
constexpr char REb=1;
constexpr char RE=2;
constexpr char REd=3;
constexpr char MIb=3;
constexpr char MI=4;
constexpr char MId=5;
constexpr char FAb=4;
constexpr char FA=5;
constexpr char FAd=6;
constexpr char SOLb=6;
constexpr char SOL=7;
constexpr char SOLd=8;
constexpr char LAb=8;
constexpr char LA=9;
constexpr char LAd=10;
constexpr char SIb=10;
constexpr char SI=11;
constexpr char SId=12;


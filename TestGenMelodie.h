#pragma once

#include <ctime>
#include <cstdlib>
#include <iostream>
#include <valarray>
#include "GammeToMIDI.h"

using namespace std;
class TestGenMelodie
{
public:
    explicit TestGenMelodie(const GammeToMIDI& gamme);;

    int nextNote();

private:
    // Dans la gamme
    int noteActuelle=0;
    int octaveActuelle=0;
    const int ecarts[7][7]
            {
                    {0,2,4,5,7,9,11},
                    {-2,0,2,3,5,7,9},
                    {-4,-2,0,1,3,5,7},
                    {-5,-3,-1,0,2,4,6},
                    {-7,-5,-3,-2,0,2,4},
                    {-9,-7,-5,-4,-2,0,2},
                    {-11,-9,-7,-6,-4,-2,0},
            };
    double getScoreEcart(int noteAvant,int noteApres) const;
    const GammeToMIDI& mGamme;
};

